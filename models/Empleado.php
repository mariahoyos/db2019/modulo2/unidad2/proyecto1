<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleado".
 *
 * @property int $IdEmpleado
 * @property string $Apellidos
 * @property string $Nombre
 * @property string $Cargo
 * @property string $Tratamiento
 * @property string $FechaNacimiento
 * @property string $FechaContratación
 * @property string $Direccion
 * @property string $Ciudad
 * @property string $Region
 * @property string $CodPostal
 * @property string $Pais
 * @property string $TelDomicilio
 * @property string $Extension
 * @property string $Foto
 * @property string $Notas
 * @property int $Jefe
 *
 * @property Empleado $jefe
 * @property Empleado[] $empleados
 * @property Pedido[] $pedidos
 */
class Empleado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FechaNacimiento', 'FechaContratación'], 'safe'],
            [['Notas'], 'string'],
            [['Jefe'], 'integer'],
            [['Apellidos'], 'string', 'max' => 40],
            [['Nombre', 'CodPostal'], 'string', 'max' => 20],
            [['Cargo'], 'string', 'max' => 60],
            [['Tratamiento'], 'string', 'max' => 50],
            [['Direccion'], 'string', 'max' => 120],
            [['Ciudad', 'Region', 'Pais'], 'string', 'max' => 30],
            [['TelDomicilio'], 'string', 'max' => 48],
            [['Extension'], 'string', 'max' => 8],
            [['Foto'], 'string', 'max' => 510],
            [['Jefe'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['Jefe' => 'IdEmpleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdEmpleado' => 'Id Empleado',
            'Apellidos' => 'Apellidos',
            'Nombre' => 'Nombre',
            'Cargo' => 'Cargo',
            'Tratamiento' => 'Tratamiento',
            'FechaNacimiento' => 'Fecha Nacimiento',
            'FechaContratación' => 'Fecha Contratación',
            'Direccion' => 'Direccion',
            'Ciudad' => 'Ciudad',
            'Region' => 'Region',
            'CodPostal' => 'Cod Postal',
            'Pais' => 'Pais',
            'TelDomicilio' => 'Tel Domicilio',
            'Extension' => 'Extension',
            'Foto' => 'Foto',
            'Notas' => 'Notas',
            'Jefe' => 'Jefe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJefe()
    {
        return $this->hasOne(Empleado::className(), ['IdEmpleado' => 'Jefe']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleado::className(), ['Jefe' => 'IdEmpleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::className(), ['IdEmpleado' => 'IdEmpleado']);
    }
}
