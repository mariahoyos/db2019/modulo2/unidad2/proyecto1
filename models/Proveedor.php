<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedor".
 *
 * @property int $IdProveedor
 * @property string $NombreEmpresa
 * @property string $NombreContacto
 * @property string $CargoContacto
 * @property string $Direccion
 * @property string $Ciudad
 * @property string $Region
 * @property string $CodPostal
 * @property string $Pais
 * @property string $Telefono
 * @property string $Fax
 * @property string $PaginaPrincipal
 *
 * @property Producto[] $productos
 */
class Proveedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PaginaPrincipal'], 'string'],
            [['NombreEmpresa'], 'string', 'max' => 80],
            [['NombreContacto', 'CargoContacto'], 'string', 'max' => 60],
            [['Direccion'], 'string', 'max' => 120],
            [['Ciudad', 'Region', 'Pais'], 'string', 'max' => 30],
            [['CodPostal'], 'string', 'max' => 20],
            [['Telefono', 'Fax'], 'string', 'max' => 48],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdProveedor' => 'Id Proveedor',
            'NombreEmpresa' => 'Nombre Empresa',
            'NombreContacto' => 'Nombre Contacto',
            'CargoContacto' => 'Cargo Contacto',
            'Direccion' => 'Direccion',
            'Ciudad' => 'Ciudad',
            'Region' => 'Region',
            'CodPostal' => 'Cod Postal',
            'Pais' => 'Pais',
            'Telefono' => 'Telefono',
            'Fax' => 'Fax',
            'PaginaPrincipal' => 'Pagina Principal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['IdProveedor' => 'IdProveedor']);
    }
}
