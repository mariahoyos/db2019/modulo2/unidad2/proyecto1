<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresastransporte".
 *
 * @property int $IdEmpresasTransporte
 * @property string $NombreEmpresa
 * @property string $Telefono
 *
 * @property Pedido[] $pedidos
 */
class Empresastransporte extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresastransporte';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NombreEmpresa'], 'string', 'max' => 80],
            [['Telefono'], 'string', 'max' => 48],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdEmpresasTransporte' => 'Id Empresas Transporte',
            'NombreEmpresa' => 'Nombre Empresa',
            'Telefono' => 'Telefono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::className(), ['IdEmpresasTransporte' => 'IdEmpresasTransporte']);
    }
}
