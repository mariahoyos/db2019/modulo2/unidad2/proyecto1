<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property string $IdCliente
 * @property string $NombreEmpresa
 * @property string $NombreContacto
 * @property string $CargoContacto
 * @property string $Direccion
 * @property string $Ciudad
 * @property string $Region
 * @property string $CodPostal
 * @property string $Pais
 * @property string $Telefono
 * @property string $Fax
 *
 * @property Pedido[] $pedidos
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdCliente'], 'required'],
            [['IdCliente'], 'string', 'max' => 10],
            [['NombreEmpresa'], 'string', 'max' => 80],
            [['NombreContacto', 'CargoContacto'], 'string', 'max' => 60],
            [['Direccion'], 'string', 'max' => 120],
            [['Ciudad', 'Region', 'Pais'], 'string', 'max' => 30],
            [['CodPostal'], 'string', 'max' => 20],
            [['Telefono', 'Fax'], 'string', 'max' => 48],
            [['IdCliente'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdCliente' => 'Id Cliente',
            'NombreEmpresa' => 'Nombre Empresa',
            'NombreContacto' => 'Nombre Contacto',
            'CargoContacto' => 'Cargo Contacto',
            'Direccion' => 'Direccion',
            'Ciudad' => 'Ciudad',
            'Region' => 'Region',
            'CodPostal' => 'Cod Postal',
            'Pais' => 'Pais',
            'Telefono' => 'Telefono',
            'Fax' => 'Fax',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        /* consulta en el modelo: Hace una relación entre el idcliente de pedidos y el idcliente de clientes */
        /* Para ver los pedidos concretos de un cliente */
        return $this->hasMany(Pedido::className(), ['IdCliente' => 'IdCliente']);
        
    }
    
      
    public function getPedidos1()
    {
        return $this->hasMany(Pedido::className(), ['IdCliente' => 'IdCliente'])->where('cargo>100');
    }
}
