<?php

/* @var $this yii\web\View */
use app\models\Cliente;
use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <?php
    
    /*
     * 1 opcion sólo find
     */
    
//    $clientes= Cliente::find()->all();
//    /* Llama al getPedidos del cliente $clientes[0] */
//    $pedidosCliente=$clientes[0]->pedidos; 
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    
    /* 
     * 2 opcion with
     */
      /* pedidos es getPedidos*/ 
      /*eagerel oading: carga los pedidos de manera temprana, no espera a que se lo pidas*/
//    $clientes=Cliente::find()->with('pedidos')->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    /*
     * 3 opcion
     * LEFT JOIN
     */
    
//    $clientes=Cliente::find()->joinWith('pedidos')->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    
    /*
     * 4 opcion
     * alias, eager loading, tipo inner join
     */
    
//    $clientes=Cliente::find()->joinWith('pedidos p',true,'INNER JOIN' )->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    /*
     * 5 opcion
     * alias lazy loading y left join
     */
    
//    $clientes=Cliente::find()->joinWith('pedidos p',false)->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    
    /*
     * 6 opcion
     * sin with: sólo tablas, no hay relaciones
     */
    
//    $clientes=Cliente::find()->join('inner join','pedido','pedido.IdCliente=cliente.IdCliente')->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    
    /*
     * 7 opcion
     */
    
//    $clientes=Cliente::find()->innerJoin('pedido p','p.IdCliente=cliente.IdCliente')->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    /*
     * 8 opcion
     */
    
//    $clientes=Cliente::find()->leftJoin('pedido p','p.IdCliente=cliente.IdCliente')->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);

     /*
     * 9 opcion
     */
    
//    $clientes=Cliente::find()->innerJoinWith('pedidos',true)->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
     /*
     * 10 opcion
     */
    
//    $pedidos= app\models\Pedido::find()->where('cargo>100');
//    /*p es un indice que hay que pasarle con un alias para la subconsulta que hay que pasar sin ejecutar*/
//    $clientes=Cliente::find()->select('p.*,cliente.*')->innerJoin(['p'=>$pedidos],'p.idcliente=cliente.idcliente')->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);

      /*
     * 11 opcion
     */
//    
//    $clientes=Cliente::find()->joinWith('pedidos')
//            ->where('cargo>100')
//            ->all();
    /* 
     * Esto produce error
    $clientes=Cliente::find()->with('pedidos')
            ->where('cargo>100')
            ->all(); 
    */
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    /*
     * 12 opcion
     */
    
//    $clientes=Cliente::find()->with(['pedidos'=>function($query){
//        $query->andWhere('Cargo>100');
//    }])->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    /*
     * 13 opcion
     * personalización de la precarga (personalizando la función? query)
     */
    
//    $clientes=Cliente::find()->joinWith(['pedidos'=>function($query){
//        $query->andWhere('Cargo>100');
//    }],false)->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    /*
     * 14 opcion
     */
    
//    $clientes=Cliente::find()->joinWith('pedidos1')->all();
//    $pedidosCliente=$clientes[0]->pedidos;
//    var_dump($clientes);
//    var_dump($pedidosCliente);
    
    /*
     * 15 opcion
     */
    
    $clientes=Cliente::find()->with('pedidos1')->all();
    $pedidosCliente=$clientes[0]->pedidos1;
    var_dump($clientes);
    var_dump($pedidosCliente);


    
    ?>
</div>
